from setuptools import setup

APP = ["vrencoder.py"]
APP_NAME = "VREncoder 0.9.3"
INCLUDES = ["tkinter"]
DATA_FILES = ["ffmpeg", "ffprobe"]
OPTIONS = {
    "argv_emulation": True,
    "iconfile": "icon.icns",
    'plist': {
        'CFBundleName': APP_NAME,
        'CFBundleDisplayName': APP_NAME,
        'CFBundleIdentifier': "com.purplepillvr.osx.vrencoder",
        'CFBundleVersion': "0.9.3",
        'CFBundleShortVersionString': "0.9.3" }
}

setup(
      app=APP,
      data_files=DATA_FILES,
      options={"py2app": OPTIONS},
      setup_requires=["py2app"],
      )
