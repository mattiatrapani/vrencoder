# -*- mode: python -*-

block_cipher = None


a = Analysis(['vrencoder.py'],
             pathex=['C:\\Users\\Nick\\Documents\\vrencoder'],
             binaries=[('C:\\Users\\Nick\\Documents\\vrencoder\\ffmpeg3.exe','Resources'),('C:\\Users\\Nick\\Documents\\vrencoder\\ffprobe3.exe','Resources')],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          exclude_binaries=False,
          name='VRencoder 0.9.3',
	  icon='icon.ico',
          debug=False,
          strip=False,
          upx=True,
          console=True )