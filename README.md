# VREncoder #

Easily encode 360º videos to the optimal codec, resolution and bitrate for the various virtual reality platforms.

### What is this repository for? ###

* This is the open-source Python code for the VREncoder project
* Version: 0.9.3
* [Learn More](https://purplepill.io/vrencoder/)

### How to build to Windows and Mac? ###

*Build OSX version using py2app*

`cd *project folder*`

`python3.5 setup.py py2app`




*Build Windows version using PyInstaller*

`cd *project folder*`

`pyinstaller vrencoder-single.spec`


### Who do I talk to? ###

* Nick Kraakman - nick@purplepill.io